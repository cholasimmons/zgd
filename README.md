***Zambian Graphic Designers project***

An open-source community project to create a fully interactive and immersive website for the *Graphic Design* community, enabling them to participate in competitions and find work from various national and international clients.

---

Feel free to visit our current [Facebook Group](https://web.facebook.com/groups/264784830273904/).